import os
import time
import json
from tencentcloud.common import credential
from tencentcloud.common.profile.client_profile import ClientProfile
from tencentcloud.common.profile.http_profile import HttpProfile
from tencentcloud.common.exception.tencent_cloud_sdk_exception import TencentCloudSDKException
from tencentcloud.dnspod.v20210323 import dnspod_client, models

import schedule
import requests
from loguru import logger


secretId = os.getenv('secretId')
secretKey = os.getenv('secretKey')
Domain = os.getenv('Domain')
SubDomain = os.getenv('SubDomain')
RecordType = os.getenv('RecordType', 'A')
RecordId = os.getenv('RecordId')
RecordLine = os.getenv('RecordLine', '默认')
DynamicTime = os.getenv('DynamicTime', '1')


def modify_dynamic_dns(domain: str, sub_domain: str, record_type: str, record_id: int, record_line: str, value: str):
    try:
        cred = credential.Credential(secretId, secretKey)
        httpProfile = HttpProfile()
        httpProfile.endpoint = "dnspod.tencentcloudapi.com"

        clientProfile = ClientProfile()
        clientProfile.httpProfile = httpProfile
        client = dnspod_client.DnspodClient(cred, "", clientProfile)

        req = models.ModifyRecordRequest()
        params = {
            "Domain": domain,
            "SubDomain": sub_domain,
            "RecordType": record_type,
            "RecordId": record_id,
            "RecordLine": record_line,
            "Value": value
        }
        req.from_json_string(json.dumps(params))

        resp = client.ModifyRecord(req)
        logger.debug(resp)

    except TencentCloudSDKException as err:
        logger.error(err)


def get_my_ip():
    url = 'https://api-2.yiwoshu.cn/api/v1/device/myip'
    try:
        r = requests.get(url=url)
        if r.status_code != 200:
            raise ValueError()
        ip = r.json()['ip']
        logger.debug(f'myip: {ip}')
    except Exception as e:
        logger.error(e)
        return None
    else:
        return ip


def main():
    myip = get_my_ip()
    if myip is None:
        return
    modify_dynamic_dns(domain=Domain, sub_domain=SubDomain, record_type=RecordType, record_id=int(RecordId),
                       record_line=RecordLine, value=myip)


schedule.every(int(DynamicTime)).minutes.do(main)
while True:
    schedule.run_pending()   # 运行所有可以运行的任务
    time.sleep(1)
